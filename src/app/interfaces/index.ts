export interface IReadings {
  id: string;
  box_id: string;
  sensor_type: string;
  unit: string;
  name: string;
  range_l: number;
  range_u: number;
  longitude: number;
  latitude: number;
  reading: number;
  reading_ts: string;
}

export interface ITableColumn {
  id: string;
  label: string;
  filterable?: boolean;
  format?: string;
  formatOptions?: string;
  orderBy?: string;
}

export interface ITableOptions {
  columns: ITableColumn[];
}
