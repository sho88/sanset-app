import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapComponent } from './components/map/map.component';
import { ReadingsComponent } from './components/readings/readings.component';

const routes: Routes = [
  { path: 'map', component: MapComponent },
  { path: 'readings', component: ReadingsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
