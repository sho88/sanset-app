import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReadingsComponent } from './components/readings/readings.component';
import { MapComponent } from './components/map/map.component';
import { TableComponent } from './components/table/table.component';
import { FormsModule } from '@angular/forms';
import { MapDirective } from './directives/map.directive';

@NgModule({
  declarations: [
    AppComponent,
    ReadingsComponent,
    MapComponent,
    TableComponent,
    MapDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
