import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IReadings } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ReadingsService {

  constructor(private readonly httpClient: HttpClient) { }

  getSensorReadings(): Observable<IReadings[]> {
    return this.httpClient.get<IReadings[]>('/assets/sensor_readings.json');
  }
}
