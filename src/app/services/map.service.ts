import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private source: BehaviorSubject<any> = new BehaviorSubject<any>({});
  private store = this.source.asObservable();

  changeCoordinates(coords: number[]): void {
    this.source.next({
      ...this.source.value,
      type: 'change:coord',
      coords,
    });
  }

  getStore(): Observable<any> {
    return this.store;
  }
}
