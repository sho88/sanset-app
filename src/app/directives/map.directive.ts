import { MapService } from 'src/app/services/map.service';
import { Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import { Map, View } from 'ol';
import { fromLonLat } from 'ol/proj';

@Directive({
  selector: '[ssMap]',
})
export class MapDirective implements OnInit {
  map!: Map;
  view!: View;
  @Input() coords!: number[];

  constructor(private readonly mapService: MapService) { }

  ngOnInit(): void {
    this.view = new View({
      center: fromLonLat([-0.12755, 51.507222]),
      zoom: 5,
    });

    this.map = new Map({
      target: 'map',
      layers: [
        new TileLayer({
          source: new XYZ({
            url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          })
        })
      ],
      view: this.view
    });

    // monitor the mapService store
    this.mapService
      .getStore()
      .subscribe((state: any) => {
        if (state.type === 'change:coord') { this.changeCoords(state.coords); }
      });
  }

  changeCoords(coordinates: number[]): void {
    this.view.animate({
      center: fromLonLat(coordinates),
      duration: 1000,
      zoom: 5
    });
  }

}
