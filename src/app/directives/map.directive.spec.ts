import { CommonModule } from '@angular/common';
import { ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MapService } from '../services/map.service';
import { MapDirective } from './map.directive';


describe('MapDirective', () => {
  let directive: MapDirective;
  let service: MapService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      providers: [MapService],
      declarations: [],
    });

    service = TestBed.inject(MapService);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });
});
