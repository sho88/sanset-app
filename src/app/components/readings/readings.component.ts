import { ITableColumn } from './../../interfaces/index';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, pluck } from 'rxjs/operators';
import { IReadings, ITableOptions } from 'src/app/interfaces';
import { ReadingsService } from 'src/app/services/readings.service';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'ss-readings',
  templateUrl: './readings.component.html',
  styleUrls: ['./readings.component.sass']
})
export class ReadingsComponent implements OnDestroy, OnInit {
  readonly source: BehaviorSubject<any> = new BehaviorSubject<any>({
    pagination: { limit: 10, pages: 1 },
    data: [],
    source: [],
  });

  source$: Observable<any> = this.source.asObservable();
  pagination$: Observable<any> = this.source$.pipe(pluck('pagination'), distinctUntilChanged());
  data$: Observable<any> = this.source$.pipe(pluck('data'), distinctUntilChanged());

  coordinates: number[] = [];
  revealMap = false;
  subscriptions: Subscription = new Subscription();
  options: ITableOptions = {
    columns: [
      { id: 'id', label: 'ID' },
      { id: 'name', label: 'Name' },
      { id: 'box_id', label: 'Box ID' },
      { id: 'sensor_type', label: 'Sensor Type', filterable: true, orderBy: 'asc' },
      { id: 'unit', label: 'Unit' },
      { id: 'range_1', label: 'Range 1', format: 'number' },
      { id: 'range_u', label: 'Range Unit' },
      { id: 'longitude', label: 'Longitude' },
      { id: 'latitude', label: 'Latitude' },
      { id: 'reading', label: 'Reading', filterable: true },
      { id: 'reading_ts', label: 'Time', format: 'date', filterable: true, orderBy: 'asc' },
    ],
  };

  constructor(
    private readonly readingsService: ReadingsService,
    private readonly mapService: MapService
  ) { }

  ngOnInit(): void {
    const sub = this.readingsService
      .getSensorReadings()
      .subscribe((readings: IReadings[]) => this.source.next({
        ...this.source.value,
        original: readings,
        source: readings,
        data: readings.slice(0, this.source.value.pagination.limit),
      }));
    this.subscriptions.add(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  // events go here -----------------------------------------------------------

  onFilter(column: ITableColumn): void {
    const source = this.source.value.source.sort((itemA: any, itemB: any) => {
      if (column.orderBy === 'asc') {
        if (itemA[column.id] > itemB[column.id]) { return 1; }
        if (itemA[column.id] < itemB[column.id]) { return -1; }
      }

      if (column.orderBy === 'desc') {
        if (itemA[column.id] < itemB[column.id]) { return 1; }
        if (itemA[column.id] > itemB[column.id]) { return -1; }
      }

      return 0;
    });
    const data = source.slice(0, this.source.value.pagination.limit);
    this.source.next({ ...this.source.value, source, data });
  }

  onRowSelect(reading: IReadings): void {
    const { latitude, longitude } = reading;
    this.mapService.changeCoordinates([latitude, longitude]);
  }

  onSearch(value: string): void {
    if (!value.length) {
      return this.source.next({
        ...this.source.value,
        source: [...this.source.value.original],
        data: [...this.source.value.original.slice(0, this.source.value.pagination.limit)],
      });
    }

    const source = this.source.value.original.filter((item: IReadings) => {
      return item.name.toLowerCase().includes(value.toLowerCase())
        || item.sensor_type.toLowerCase().includes(value.toLowerCase());
    });
    const data = source.slice(0, this.source.value.pagination.limit);
    this.source.next({ ...this.source.value, source, data });
  }

}
