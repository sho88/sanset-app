import { TableComponent } from './../table/table.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IReadings } from 'src/app/interfaces';
import { MapService } from 'src/app/services/map.service';

import { ReadingsComponent } from './readings.component';
import { FormsModule } from '@angular/forms';

fdescribe('ReadingsComponent', () => {
  let component: ReadingsComponent;
  let fixture: ComponentFixture<ReadingsComponent>;
  let service: MapService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommonModule, FormsModule, HttpClientModule],
      providers: [MapService],
      declarations: [ReadingsComponent, TableComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadingsComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(MapService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use service to send off coordinates.', () => {
    spyOn(service, 'changeCoordinates');

    component.onRowSelect({
      latitude: -23.001,
      longitude: 1.309,
    } as IReadings);

    expect(service.changeCoordinates).toHaveBeenCalled();
  });
});
