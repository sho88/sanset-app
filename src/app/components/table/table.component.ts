import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { IReadings, ITableColumn, ITableOptions } from 'src/app/interfaces';

@Component({
  selector: 'ss-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent implements OnInit {
  @Input() data$!: Observable<any[]>;
  @Input() pagination$!: Observable<IReadings[]>;
  @Input() options!: ITableOptions;
  @Output() filter: EventEmitter<ITableColumn> = new EventEmitter<ITableColumn>();
  @Output() rowSelect: EventEmitter<IReadings> = new EventEmitter<IReadings>();
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  query = '';

  constructor() { }

  ngOnInit(): void {
  }

  // events go here -----------------------------------------------------------

  onFilter(column: ITableColumn): void {
    if (!column.filterable) {
      return;
    }

    this.filter.emit(column);
  }

  onKeyUp(value: any): void {
    this.search.emit(value);
  }

  onRowSelect(reading: IReadings): void {
    this.rowSelect.emit(reading);
  }

}
