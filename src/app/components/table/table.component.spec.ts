import { IReadings } from 'src/app/interfaces';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit reading to parent component.', () => {
    spyOn(component.rowSelect, 'emit');
    const reading = {} as IReadings;
    component.onRowSelect(reading);
    expect(component.rowSelect.emit).toHaveBeenCalledWith(reading);
  });

});
